<?php

namespace spectrum;

/**
 * Created by PhpStorm.
 * User: yjradeh
 * Date: 11/24/17
 * Time: 10:49 AM
 */

use GuzzleHttp\Client;

define( "BASE_URL", "https://api.sport1.de/api/sports/" );

class NetworkManager {

	public function getMatchesBySeasonAndTeam( $seasonId, $teamId ) {

		$client = new Client();
		$res    = $client->get(
			BASE_URL . "matches-by-season-team/co12/se$seasonId/te$teamId"
		);

		return json_decode( $res->getBody() );
	}

	public function getMatchEvent( $matchId ) {

		$client = new Client();
		$res    = $client->get(
			BASE_URL . "match-event/ma$matchId"
		);
		return json_decode( $res->getBody() );
	}

}