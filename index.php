<?php

/**
 * Created by PhpStorm.
 * User: yjradeh
 * Date: 11/24/17
 * Time: 10:49 AM
 */

require_once __DIR__ . '/vendor/autoload.php';

use spectrum\NetworkManager;

$teamId   = 9;
$seasonId = 20812;

$networkManager = new NetworkManager();

//get all matches
$matches = $networkManager->getMatchesBySeasonAndTeam( $seasonId, $teamId );

//loop through matches get the next/current match
$now = \Carbon\Carbon::now( 'Europe/Berlin' );

//have a fake date for testing purpose
$matches->match[0]->match_date = '24.11.2017';
$matches->match[0]->match_time = '10:40';
$matches->match[0]->finished   = 'no';

$currentMatches = [];

foreach ( $matches->match AS $match ) {

	//ignore all finished matched, as it is already returned by the API
	if ( $match->finished == 'yes' ) {
		continue;
	}

	//get match date and calculate the difference
	$matchDateTime = \Carbon\Carbon::createFromFormat( 'd.m.Y H:i', $match->match_date . ' ' . $match->match_time );
	$diffInMinutes = $now->diffInMinutes( $matchDateTime );

	//there is no need to compare the dates here since we are using the finished properties
	if ( $diffInMinutes < 120 ) {
		$matchDetails = $networkManager->getMatchEvent( $match->id );

		//loop on players
		foreach ( $matchDetails AS $player ) {
			//get team player by roles
			if ( ! empty( $player->team ) && ! empty( $player->team_person ) && ! empty( $player->person ) ) {
				$currentMatches[ $match->id ][ $player->team->name ][ $player->team_person->role->name ][] = $player->person->name;
			}
		}
	}
}

foreach ( $currentMatches AS $match ) {
	$teamsNames = array_keys( $match );
	echo "<b>" . implode( $teamsNames, " X " ) . "</b>";
	echo '<br>------------------------------------<br>';
	foreach ( $match AS $teamName => $roles ) {
		echo $teamName . " : ";
		echo '<br>-------<br>';
		foreach ( $roles AS $roleName => $players ) {
			echo "<b>" . $roleName . "</b> : ";
			echo implode( $players, " , " );
			echo '<br>';
		}
		echo '<br><br>';
	}
}